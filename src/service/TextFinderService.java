package service;

import controller.*;
import view.*;
import model.Lyric;
import utils.*;
import java.io.File;

public class TextFinderService{
	private static int flowCount;
	private static String wordToFind;
	private static Lyric lyric = new Lyric();
	
	public static void start(){
		while (flowCount<5){
			switch (flowCount){
				case 0:
					initializeApplication();
					break;
				case 1:
					chooseLyricFile();
					break;
				case 2:
					chooseWordToFind();
					break;
				case 3:
					findWordInsideLyric();
					break;
				case 4:
					seeLyric();
					break;
			}	
		}
		GeneralView.showGoodbyeMessage();		
	}
	
	private static void initializeApplication(){
		Lyric lyric = new Lyric();
		FormView.showContinueOption();
		flowCount += InputOption.optionContinue(1);
		if (flowCount < 0){
			flowCount = 0;
		}
	}
	
	private static void chooseLyricFile(){
		FormView.showLyricPathForm();
		lyric.setLyricFile(InputForm.inputLyricFile());
		InputOption.optionBuffer();
		FormView.showOptionPath();
		int choice = InputOption.optionContinue(3);
		if (choice == 3){
			flowCount = 1;
			return;
		}
		flowCount += choice;
	}
	
	private static void chooseWordToFind(){
		FormView.showWordFindForm();
		wordToFind = InputForm.inputWord();
		InputOption.optionBuffer();
		FormView.showOptionFind();
		int choice = InputOption.optionContinue(2);
		if (choice == 2){
			flowCount = 1;
			return;
		}
		flowCount += choice;
	}
	
	private static void findWordInsideLyric(){
		int totalWord = TextFinderUtil.findWordInLyric(wordToFind, lyric.getLyricFile());
		TextFinderView.viewTotalWordResult(totalWord);
		int choice = InputOption.optionContinue(3);
		if (choice == 2 || choice == 3){
			flowCount = (flowCount - choice) + 1;
			System.out.println(flowCount);
			return;
		}
		flowCount += choice;
	}
	
	private static void seeLyric(){
		TextFinderView.viewLyric(lyric.getLyricFile());
		InputOption.optionBuffer();
		FormView.showOptionRestart();
		int choice = InputOption.optionContinue(1);
		if (choice == 1){
			flowCount = 0;
			return;
		}
		flowCount += choice;
	}
}