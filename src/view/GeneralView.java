package view;

public class GeneralView {
		
	public static void showGoodbyeMessage(){
		showAppDescription();
		System.out.printf("|          The application is stopped!          |\n");
		System.out.printf("|                                               |\n");
		System.out.printf("|     Thank you for using our application!      |\n");
		showBottomBorder();	
	}
	
	protected static void showAppDescription(){
		clearTerminalDisplay();
		System.out.printf("=================================================\n");
		System.out.printf("|                                               |\n");
		System.out.printf("|       Welcome to the text finder app!         |\n");
		System.out.printf("|                                               |\n");
		System.out.printf("| Use this app to search your choosen word      |\n");
		System.out.printf("| inside the song_lyrics.txt file!              |\n");
		System.out.printf("|                                               |\n");
	}
	
	protected static void showBottomBorder(){
		System.out.printf("|                                               |\n");
		System.out.printf("=================================================\n");
	}
	
	private static void clearTerminalDisplay(){
		try {
			if (System.getProperty("os.name").contains("Windows")){
				new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
			}
			else {
				Runtime.getRuntime().exec("clear");
			}
		}
		catch (Exception exception){
			System.out.printf("Sorry the terminal can't be cleared because you're currently using unknown OS!");
		}
	}
}