package view;

public class FormView extends GeneralView{
	public static void showContinueOption(){
		showAppDescription();
		System.out.printf("| Do you want to continue?                      |\n");
		System.out.printf("| (1) Yes                                       |\n");
		showAppGeneralMenu();
	}
	
	public static void showLyricPathForm(){
		showAppDescription();
		System.out.printf("| Please input your lyric file path!            |\n");
		System.out.printf("| Press [ENTER] when you're done!               |\n");
		showBottomBorder();
	}
	
	public static void showWordFindForm(){
		showAppDescription();
		System.out.printf("| Please input your word (single word only)!    |\n");
		System.out.printf("| Press [ENTER] when you're done!               |\n");
		showBottomBorder();
	}
	
	public static void showOptionPath(){
		showAppDescription();
		System.out.printf("| Choose these number choices before continue   |\n");
		System.out.printf("| (1) Continue choose word to find              |\n");
		System.out.printf("| (2) Found match word result                   |\n");
		System.out.printf("| (3) Change lyric file path                    |\n");
		showAppGeneralMenu();
	}
	
	public static void showOptionFind(){
		showAppDescription();
		System.out.printf("| Choose these number choices before continue   |\n");
		System.out.printf("| (1) Continue find word inside lyric           |\n");
		System.out.printf("| (2) Change lyric file path                    |\n");
		showAppGeneralMenu();
	}
	
	public static void showOptionRestart(){
		showAppDescription();
		System.out.printf("| Choose these number choices                   |\n");
		System.out.printf("| (1) Restart app                               |\n");
		showAppGeneralMenu();
	}
	
	public static void showAppGeneralMenu(){
		System.out.printf("|                                               |\n");
		System.out.printf("| (0) Back                                      |\n");
		System.out.printf("| (99) Close the app                            |\n");
		showBottomBorder();
	}
}