package view;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;

public class TextFinderView extends GeneralView{
	public static void viewTotalWordResult(int totalWord){
		showAppDescription();
		if (totalWord == 0){
			System.out.printf("| Sorry, we can't found your word inside your   |\n");
			System.out.printf("| choosen lyric file                            |\n");
		}
		else {
			System.out.printf("| We found %-4d matching word inside your       |\n", totalWord);
			System.out.printf("| choosen lyric                                 |\n");
		}		
		System.out.printf("|                                               |\n");
		System.out.printf("| Choose these number choices before continue   |\n");
		System.out.printf("| (1) View lyric                                |\n");
		System.out.printf("| (2) Change word to find                       |\n");
		System.out.printf("| (3) Change lyric file path                    |\n");
		FormView.showAppGeneralMenu();
	}
	
	public static void viewLyric(File lyricFile){
		try{
			BufferedReader bufferedReader = new BufferedReader(new FileReader(lyricFile));
			String lyricLine;
			showAppDescription();
			System.out.printf("| Here is your choosen lyric file               |\n");
			System.out.printf("| Enjoy the song and keep karaoke-ing!          |\n");
			showBottomBorder();
			System.out.printf("\n");
			while ((lyricLine = bufferedReader.readLine()) != null){
				System.out.println(lyricLine);
			}
		}
		catch (Exception e){
			System.out.printf("\nFile not found!\n");
		}
		
	}
}