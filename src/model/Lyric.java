package model;

import java.io.File;

public class Lyric{
	private File lyricFile;
	
	public void setLyricFile(File lyricFile){
		this.lyricFile = lyricFile;
	}
	
	public File getLyricFile(){
		return this.lyricFile;
	}
}