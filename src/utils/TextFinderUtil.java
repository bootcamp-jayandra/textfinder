package utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.util.regex.*;

public class TextFinderUtil{
	public static int findWordInLyric(String word, File lyricFile){
		String lyricLine;
		int matchCounter = 0;
		try{
			BufferedReader bufferedReader = new BufferedReader(new FileReader(lyricFile));
			String regex = String.format("\\b%s\\b", word);
			Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
			while ((lyricLine = bufferedReader.readLine()) != null){
				Matcher match = pattern.matcher(lyricLine);
				while (match.find()){
					matchCounter++;
				}
			}
		}
		catch (Exception e){
			System.out.printf("\nFile not found!\n");
		}
		return matchCounter;
	}
}