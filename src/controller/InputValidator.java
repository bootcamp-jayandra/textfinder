package controller;

import java.util.Scanner;
import java.util.regex.*;

public class InputValidator{
	protected static Scanner scan = new Scanner(System.in);
	private static Pattern pattern;
	private static Matcher match;
		
	protected static int safeInteger(){
		while (true){
			try {
				return Integer.valueOf(scan.nextLine());
			}
			catch (Exception e){
				System.out.printf("Enter the correct integer: ");
			}
		}
	}
	
	protected static String safeWordString(){
		pattern = Pattern.compile("[^\\w-']+");
		String wordString;
		do {
			wordString = scan.nextLine();
			match = pattern.matcher(wordString);
			if (match.find()){
				System.out.printf("Enter one word only: ");
			}
		}
		while (match.find());
		return wordString;
	}
	
	protected static String safePathString(){
		pattern = Pattern.compile("[*?\"<>|]");
		String pathString;
		do {
			pathString = scan.nextLine();
			match = pattern.matcher(pathString);
			if (match.find()){
				System.out.printf("Path can't contain [*?\"<>|] character!\n");
				System.out.printf("Please enter new correct path: ");
			}
		}
		while (match.find());
		return pathString;
	}
}