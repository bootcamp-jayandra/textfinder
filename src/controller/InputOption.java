package controller;

public class InputOption extends InputValidator{
	public static int optionContinue(int choiceLimit){
		int choice;
		System.out.printf("\nYour number of choice: ");
		do {
			choice = safeInteger();
			if (choice != 99 && (choice < 0 || choice > choiceLimit)){
				System.out.printf("Input the proper number of choice: ");
			}
		}
		while (choice != 99 && (choice < 0 || choice > choiceLimit));
		System.out.println(choice);
		return optionBackOrEnd(choice);
	}
	
	public static void optionBuffer(){
		System.out.printf("\n          ;;PRESS [ENTER] TO CONTINUE;;");
		scan.nextLine();
	}
	
	private static int optionBackOrEnd(int choice){
		if (choice == 0) {
			return -1;
		}
		return choice;
	}
}