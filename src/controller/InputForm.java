package controller;

import java.io.File;

public class InputForm extends InputValidator{
	public static File inputLyricFile(){
		System.out.printf("\nPlease enter lyric path (.txt): ");
		String lyricPath;
		int pathLength;
		File lyricFile = new File("");
		do {
			lyricPath = safePathString();
			pathLength = lyricPath.length();
			lyricFile = new File(lyricPath);
			if (pathLength < 4 || !lyricPath.substring(pathLength-4, pathLength).equalsIgnoreCase(".txt")
				|| !lyricFile.exists()){
				System.out.printf("Please enter the correct lyric path (.txt): ");
			}
		}
		while (pathLength < 4 || !lyricPath.substring(pathLength-4, pathLength).equalsIgnoreCase(".txt")
				|| !lyricFile.exists());
		return lyricFile;
	}
	
	public static String inputWord(){
		System.out.printf("\nPlease enter word to find (single word only): ");
		return safeWordString();
	}
}